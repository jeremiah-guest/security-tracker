An LTS security update is needed for the following source packages.
When you add a new entry, please keep the list alphabetically sorted.

The specific CVE IDs do not need to be listed, they can be gathered in an up-to-date manner from
https://security-tracker.debian.org/tracker/source-package/SOURCEPACKAGE
when working on an update.

To pick an issue, simply add your name behind it. To learn more about how
this list is updated have a look at
https://wiki.debian.org/LTS/Development#Triage_new_security_issues

--
evolution
--
faad2 (Hugo Lefeuvre)
  NOTE: 20190225: No known patch yet. Going to fix the most exploitable issues at first.
  NOTE: CVE-2018-20362: wrote a patch, currently testing it. This might fix many other
  NOTE: issues at the same time.
--
firmware-nonfree (Emilio)
  NOTE: 20190304: https://lists.debian.org/debian-lts/2019/03/msg00000.html
--
gnutls28
--
ikiwiki
--
imagemagick (Roberto C. Sánchez)
  NOTE: 20181227: We should address the many open issues in imagemagick either
  NOTE: by patching them separetely as we did in Wheezy or by updating to a
  NOTE: new upstream version like the security team did with Graphicsmagick in
  NOTE: Stretch. (apo)
--
kde4libs (Hugo Lefeuvre)
--
libav
  NOTE: 20190131: Re-added after ~deb8u5 upload. Still not done, yet.
--
liblivemedia
  NOTE: 20190226: CVE-2019-773{2,3}: wait for upstream patch - hle
--
libraw (Thorsten Alteholz)
  NOTE: 20181222: As usual please consider to fix ignored/no-dsa issues too,
  NOTE: especially those that are still marked vulnerable in Stretch but also
  NOTE: the stack-based and heap-based overflow issues. (apo)
  NOTE: 20190114: Ton of issues, I couldn't reproduce most of them. CVE-2017-13735 
  NOTE: is reproducible even after upstream patch.
  NOTE: 20190202: Marked CVE-2017-14348, CVE-2018-20337, CVE-2018-20363, CVE-2018-20364
  NOTE: and CVE-2018-20365 as no DSA.
--
libsdl1.2 (Abhijith PA)
--
libsdl2 (Abhijith PA)
--
libsndfile (Emilio)
 NOTE: 20190219: CVE-2018-19758 has been "fixed" so this package is affected by CVE-2019-3832
--
libsolv
  NOTE: 20190127: maintainer is Mike Gabriel
--
linux (Ben Hutchings)
--
linux-4.9 (Ben Hutchings)
--
mysql-connector-python
  NOTE: 20190202: Oracle stuff. Details are not disclosed. Requires update to
  NOTE: supported version.
--
nettle (Sylvain Beucler)
  NOTE: 20190119: Prerequisite for gnutls28 being fixed.
--
openjdk-7 (Emilio)
  NOTE: 20190304: updating to 7u211
--
openssh (Mike Gabriel)
  NOTE: 20190227: Work in progress. First draft is still vulnerable to PoC: https://www.exploit-db.com/exploits/46193
  NOTE: 20190227: Problematic is that jessie's / wheezy's versions don't have the utf8.(c|h) code, yet. Probably needs to be backported.
  NOTE: 20190228: CVE-2019-6111 seemingly not-yet-fixed, see https://bugs.debian.org/923486
  NOTE: 20190228: Package draft for jessie LTS locally, but the CVE-2019-6111 patch requires being fixed first before proceeding
--
php5 (Thorsten Alteholz)
--
polarssl
  NOTE: 20121207: Not 100% sure if vulnerable. Upstream would prefer us to move to latest version, etc. (!). (lamby)
--
poppler (Markus Koschany)
--
qemu
  NOTE: CVE-2018-19665: wait for final patch
--
rdflib
  NOTE: Maintainer not contacted. Follow the debian bug about status. This should probably be fixed.
--
sox
  NOTE: 20190305: CVE-2019-835{4,5,6,7} no upstream patch yet, might take some time.
  NOTE: Check again later. - hle
--
sqlalchemy
  NOTE: 20190226: Patch is quite invasive. (lamby)
--
sssd (Hugo Lefeuvre)
--
symfony (Roberto C. Sánchez)
  NOTE: 20190302: Upload is ready; sent call for testing to the mailing list (roberto)
--
wireshark (Thorsten Alteholz)
--
wordpress
--
xen (worked on by credative)
--
zabbix
  NOTE: 20190302: I think we should upgrade to version 2.2.23 because we ship a
  NOTE: LTS release of Zabbix in Jessie. This way we will get all fixes with minimal risks. (apo)
--
